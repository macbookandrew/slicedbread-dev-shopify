Introduction
============

Demo store for proficiency test.

Local Development
=================

Run `theme watch --notify=/var/tmp/theme_ready & browser-sync start --proxy="https://sliced-bread-dev.myshopify.com/?nopreview" --files="/var/tmp/theme_ready" --reloadDelay=300 --open="external" --host="andrews-macbook-pro.local" &`

Instagram App Notes
===================

- Create an Instagram app
- Disable the “Disable implicit OAuth” checkbox since this is all JS client-side
- Find Instagram user id here: `https://smashballoon.com/instagram-feed/find-instagram-user-id/`
- Get an OAuth access token
  - GUI: go to [http://instagram.pixelunion.net/](instagram.pixelunion.net) and follow the prompts
  - Manual: go to  `https://api.instagram.com/oauth/authorize/?client_id=[clientID]&redirect_uri=https://eatyour-coffee.myshopify.com&response_type=token`
  - After approving access, you will be redirected to https://eatyour-coffee.myshopify.com/#access_token=**1341682575.9e1a0cd.ece7823fce6147eab20711bd7afaba33**; the bold section is the token
- Update the token in the [theme editor](https://eatyour-coffee.myshopify.com/admin/themes/7487225892/editor)
  - Go to the “Theme settings” tab
  - Go to the “Social media” item
  - Scroll down to the “Instagram API” section and add it in the “Instagram oAuth Access Token” field
  - Press the “Save” button at the top right of the screen
